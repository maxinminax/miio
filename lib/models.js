/**
 * Mapping from models into high-level devices.
 */
const AirMonitor = require('./devices/air-monitor');
const AirPurifier = require('./devices/air-purifier');
const Gateway = require('./devices/gateway');

const Vacuum = require('./devices/vacuum');

const PowerPlug = require('./devices/power-plug');
const PowerStrip = require('./devices/power-strip');

const Humidifier = require('./devices/humidifier');

const YeelightColor = require('./devices/yeelight.color');
const YeelightMono = require('./devices/yeelight.mono');

module.exports = {
	'zhimi.airmonitor.v1': AirMonitor,

	// Air Purifier 1 (and Pro?)
	'zhimi.airpurifier.v1': AirPurifier,
	'zhimi.airpurifier.v2': AirPurifier,
	'zhimi.airpurifier.v3': AirPurifier,
	'zhimi.airpurifier.v6': AirPurifier,

	// Air Purifier 2
	'zhimi.airpurifier.m1': AirPurifier,
	'zhimi.airpurifier.m2': AirPurifier,

	// Air Purifier 2S
	'zhimi.airpurifier.ma2': AirPurifier,

	'zhimi.humidifier.v1': Humidifier,

	'chuangmi.plug.m1': PowerPlug,
	'chuangmi.plug.v1': require('./devices/chuangmi.plug.v1'),
	'chuangmi.plug.v2': PowerPlug,

	'rockrobo.vacuum.v1': Vacuum,
	'roborock.vacuum.s5': Vacuum,

	'lumi.gateway.v2': Gateway.WithLightAndSensor,
	'lumi.gateway.v3': Gateway.WithLightAndSensor,
	'lumi.acpartner.v1': Gateway.Basic,
	'lumi.acpartner.v2': Gateway.Basic,
	'lumi.acpartner.v3': Gateway.Basic,
  'lumi.airer.acn01': require('./devices/airer'),

	'qmi.powerstrip.v1': PowerStrip,
	'zimi.powerstrip.v2': PowerStrip,

	'yeelink.light.lamp1': YeelightMono,
	'yeelink.light.mono1': YeelightMono,
	'yeelink.light.color1': YeelightColor,
	'yeelink.light.strip1': YeelightColor,
  'yeelink.light.strip2': YeelightColor,
  'yeelink.light.ceiling3': require('./devices/yeelight.night-light'),
  'yeelink.light.ceiling9': require('./devices/yeelight.night-light'),

	'philips.light.sread1': require('./devices/eyecare-lamp2'),
	'philips.light.bulb': require('./devices/philips-light-bulb'),

  'zhimi.aircondition.v1': require('./devices/air-conditioner'),
  'zhimi.aircondition.za2': require('./devices/air-conditioner'),
  'zhimi.fan.za4': require('./devices/smartmi-fan'),
  'zhimi.airpurifier.ma4': require('./devices/air-purifier.miot'),
  'viomi.bhf_light.v3': require('./devices/viomi-bath-heater.miot'),
  'viomi.oven.so1': require('./devices/viomi-oven'),
  'viomi.hood.c2': require('./devices/viomi-hood'),
  'viomi.vacuum.v7': require('./devices/viomi-vacuum'),
  'viomi.dishwasher.v03': require('./devices/viomi-dishwasher'),
  'viomi.juicer.v2': require('./devices/viomi-juicer'),
  'viomi.waterheater.e7': require('./devices/viomi-water-heater'),
  'yunmi.waterpuri.s4': require('./devices/viomi-water-purifier'),
  'msj.f_washer.m1': require('./devices/fruit-purifier'),
  'imibar.cooker.mbihr3': require('./devices/viomi-cooker'),
	'leshow.fan.ss4': require('./devices/leshow-fan'),
	'ows.towel_w.mj1x0': require('./devices/towel-rack')
};

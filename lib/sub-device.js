const { Thing } = require('abstract-things');

module.exports = Thing.type(
	(Parent) =>
		class extends Parent {
			static get type() {
				return ['miio', 'miio:sub-device'];
			}

			constructor(parent, id) {
				super();
				this.parent = parent;
				this.internalId = id;
				this.id = this.parent.id + ':' + this.internalId;

				// Hijack the property updated event to make control easier
				const propertyUpdated = this.parent.propertyUpdated;
				this.parent.propertyUpdated = (key, value, oldValue) => {
					propertyUpdated.call(this.parent, key, value, oldValue);

					const internalKey = this.getInternalPropertyName(key);
					if (internalKey) {
						this.propertyUpdated(internalKey, value, oldValue);
					}
				};
			}

			getParentPropertyName(property) {
				return this.internalId + ':' + property;
			}

			getInternalPropertyName(property) {
				const regex = new RegExp(`^${this.internalId}:`);
				if (property.match(regex)) {
					return property.replace(regex, '');
				}
				return null;
			}

			defineProperty(name, def = {}) {
				def.name = this.getParentPropertyName(def.name || name);
				this.parent.defineProperty(name, def);
			}

			propertyUpdated(key, value, oldValue) {}
		}
);

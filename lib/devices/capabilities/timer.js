const Thing = require('abstract-things/thing');
const State = require('abstract-things/common/state');
const { duration } = require('abstract-things/values');

module.exports = Thing.mixin(Parent => class extends Parent.with(State) {

	static get capability() {
		return 'target-timer';
	}

	static availableAPI(builder) {
		builder.event('timerChanged')
			.type('timer')
			.description('The target timer has changed')
			.done();

		builder.action('timer')
			.description('Get the target timer')
			.returns('timer', 'The target timer')
			.done();
	}

	timer() {
		return Promise.resolve(this.getState('timer'));
	}

	updateTimer(target) {
		target = duration(target);

		if(this.updateState('timer', target)) {
			this.emitEvent('timerChanged', target);
		}
	}
});
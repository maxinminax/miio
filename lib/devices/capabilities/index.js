const {
  Thing,
  ErrorState,
  Status,
  Power,
  SwitchablePower,
  Mode,
  SwitchableMode,
  NightMode,
  SwitchableNightMode,
  BatteryLevel,
  ChargingState,
  AutonomousCharging,
  AudioFeedback,
  SwitchableAudioFeedback,
  Led,
  SwitchableLed,
  LedLevel,
  SwitchableLedLevel,
  Position,
  AdjustablePosition,
  Language,
  SwitchableLanguage
} = require('abstract-things');

const {
  TargetHumidity,
  AdjustableTargetHumidity,
  TargetTemperature,
  AdjustableTargetTemperature,
  FanSpeed,
  AdjustableFanSpeed,
  FanLevel,
  SwitchableFanLevel,
  WaterLevel,
  SwitchableWaterLevel,
  CleaningState,
  AutonomousCleaning,
  SpotCleaning,
  VerticalSwing,
  SwitchableVerticalSwing,
  HorizontalSwing,
  SwitchableHorizontalSwing
} = require('abstract-things/climate');

const {
  Brightness,
  Dimmable,
  Colorable
} = require('abstract-things/lights');

const {
  Volume,
  AdjustableVolume,
  Muted,
  Muteable
} = require('abstract-things/media');

const {
  CookingState,
  AutonomousCooking
} = require('abstract-things/appliances');

const {
  AtmosphericPressure,
  CarbonDioxideDetection,
  CarbonDioxideLevel,
  CarbonMonoxideDetection,
  CarbonMonoxideLevel,
  ContactDetection,
  Illuminance,
  MotionDetection,
  PM2_5,
  PM10,
  PowerConsumed,
  PowerLoad,
  RelativeHumidity,
  RelativeTemperature,
  SmokeDetection,
  Voltage,
  WaterDetection,
  TDS
} = require('abstract-things/sensors');

function bind(Type, updateName, property) {
	return Thing.mixin((Parent) => class extends Parent.with(Type) {
      propertyUpdated(key, value) {
        if (key === property) {
          this[updateName](value);
        }

        super.propertyUpdated(key, value);
      }
    }
	);
}

const TimeRemaining = require('./time-remaining');
const Timer = require('./timer');
const AdjustableTimer = require('./adjustable-timer');

module.exports.TimeRemaining = bind(TimeRemaining, 'updateTimeRemaining', 'timeRemaining');
module.exports.Timer = bind(Timer, 'updateTimer', 'timer');
module.exports.AdjustableTimer = bind(AdjustableTimer, 'updateTimer', 'timer');

module.exports.ErrorState = bind(ErrorState, 'updateError', 'error');
module.exports.Status = bind(Status, 'updateStatus', 'status');
module.exports.Power = bind(Power, 'updatePower', 'power');
module.exports.SwitchablePower = bind(SwitchablePower, 'updatePower', 'power');
module.exports.Mode = bind(Mode, 'updateMode', 'mode');
module.exports.SwitchableMode = bind(SwitchableMode, 'updateMode', 'mode');
module.exports.NightMode = bind(NightMode, 'updateNightMode', 'nightMode');
module.exports.SwitchableNightMode = bind(SwitchableNightMode, 'updateNightMode', 'nightMode');
module.exports.BatteryLevel = bind(BatteryLevel, 'updateBatteryLevel', 'batteryLevel');
module.exports.ChargingState = bind(ChargingState, 'updateCharge', 'charge');
module.exports.AutonomousCharging = bind(AutonomousCharging, 'updateCharge', 'charge');
module.exports.AudioFeedback = bind(AudioFeedback, 'updateAudioFeedback', 'audioFeedback');
module.exports.SwitchableAudioFeedback = bind(SwitchableAudioFeedback, 'updateAudioFeedback', 'audioFeedback');
module.exports.Led = bind(Led, 'updateLed', 'led');
module.exports.SwitchableLed = bind(SwitchableLed, 'updateLed', 'led');
module.exports.LedLevel = bind(LedLevel, 'updateLedLevel', 'ledLevel');
module.exports.SwitchableLedLevel = bind(SwitchableLedLevel, 'updateLedLevel', 'ledLevel');
module.exports.Position = bind(Position, 'updatePosition', 'position');
module.exports.AdjustablePosition = bind(AdjustablePosition, 'updatePosition', 'position');
module.exports.Language = bind(Language, 'updateLanguage', 'language');
module.exports.SwitchableLanguage = bind(SwitchableLanguage, 'updateLanguage', 'language');

module.exports.TargetHumidity = bind(TargetHumidity, 'updateTargetHumidity', 'targetHumidity');
module.exports.AdjustableTargetHumidity = bind(AdjustableTargetHumidity, 'updateTargetHumidity', 'targetHumidity');
module.exports.TargetTemperature = bind(TargetTemperature, 'updateTargetTemperature', 'targetTemperature');
module.exports.AdjustableTargetTemperature = bind(AdjustableTargetTemperature, 'updateTargetTemperature', 'targetTemperature');
module.exports.FanSpeed = bind(FanSpeed, 'updateFanSpeed', 'fanSpeed');
module.exports.AdjustableFanSpeed = bind(AdjustableFanSpeed, 'updateFanSpeed', 'fanSpeed');
module.exports.FanLevel = bind(FanLevel, 'updateFanLevel', 'fanLevel');
module.exports.SwitchableFanLevel = bind(SwitchableFanLevel, 'updateFanLevel', 'fanLevel');
module.exports.WaterLevel = bind(WaterLevel, 'updateWaterLevel', 'waterLevel');
module.exports.SwitchableWaterLevel = bind(SwitchableWaterLevel, 'updateWaterLevel', 'waterLevel');
module.exports.CleaningState = bind(CleaningState, 'updateCleaning', 'cleaning');
module.exports.AutonomousCleaning = bind(AutonomousCleaning, 'updateCleaning', 'cleaning');
module.exports.SpotCleaning = bind(SpotCleaning, 'updateCleaning', 'cleaning');
module.exports.VerticalSwing = bind(VerticalSwing, 'updateVerticalSwing', 'verticalSwing');
module.exports.SwitchableVerticalSwing = bind(SwitchableVerticalSwing, 'updateVerticalSwing', 'verticalSwing');
module.exports.HorizontalSwing = bind(HorizontalSwing, 'updateHorizontalSwing', 'horizontalSwing');
module.exports.SwitchableHorizontalSwing = bind(SwitchableHorizontalSwing, 'updateHorizontalSwing', 'horizontalSwing');

module.exports.Brightness = bind(Brightness, 'updateBrightness', 'brightness');
module.exports.Dimmable = bind(Dimmable, 'updateBrightness', 'brightness');
module.exports.Colorable = bind(Colorable, 'updateColor', 'color');

module.exports.Volume = bind(Volume, 'updateVolume', 'volume');
module.exports.AdjustableVolume = bind(AdjustableVolume, 'updateVolume', 'volume');
module.exports.Muted = bind(Muted, 'updateMuted', 'muted');
module.exports.Muteable = bind(Muteable, 'updateMuted', 'muted');

module.exports.CookingState = bind(CookingState, 'updateCooking', 'cooking');
module.exports.AutonomousCooking = bind(AutonomousCooking, 'updateCooking', 'cooking');

module.exports.AtmosphericPressure = bind(AtmosphericPressure, 'updateAtmosphericPressure', 'atmosphericPressure');
module.exports.CarbonDioxideDetection = bind(CarbonDioxideDetection, 'updateCarbonDioxideDetected', 'carbonDioxideDetected');
module.exports.CarbonDioxideLevel = bind(CarbonDioxideLevel, 'updateCarbonDioxideLevel', 'carbonDioxideLevel');
module.exports.CarbonMonoxideDetection = bind(CarbonMonoxideDetection, 'updateCarbonMonoxideDetected', 'carbonMonoxideDetected');
module.exports.CarbonMonoxideLevel = bind(CarbonMonoxideLevel, 'updateCarbonMonoxideLevel', 'carbonMonoxideLevel');
module.exports.ContactDetection = bind(ContactDetection, 'updateContactDetected', 'contactDetected');
module.exports.Illuminance = bind(Illuminance, 'updateIlluminance', 'illuminance');
module.exports.MotionDetection = bind(MotionDetection, 'updateMotionDetected', 'motionDetected');
module.exports.PM2_5 = bind(PM2_5, 'updatePM2_5', 'pm2_5');
module.exports.PM10 = bind(PM10, 'updatePM10', 'pm10');
module.exports.PowerConsumed = bind(PowerConsumed, 'updatePowerConsumed', 'powerConsumed');
module.exports.PowerLoad = bind(PowerLoad, 'updatePowerLoad', 'powerLoad');
module.exports.RelativeHumidity = bind(RelativeHumidity, 'updateRelativeHumidity', 'relativeHumidity');
module.exports.RelativeTemperature = bind(RelativeTemperature, 'updateRelativeTemperature', 'relativeTemperature');
module.exports.SmokeDetection = bind(SmokeDetection, 'updateSmokeDetected', 'smokeDetected');
module.exports.Voltage = bind(Voltage, 'updateVoltage', 'voltage');
module.exports.WaterDetection = bind(WaterDetection, 'updateWaterDetected', 'waterDetected');
module.exports.TDS = bind(TDS, 'updateTDS', 'tds');
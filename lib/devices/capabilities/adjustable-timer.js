const Thing = require('abstract-things/thing');
const { duration } = require('abstract-things/values');
const Timer = require('./timer');

module.exports = Thing.mixin(Parent => class extends Parent.with(Timer) {

  static get capability() {
    return 'adjustable-timer';
  }

  static availableAPI(builder) {
    builder.action('timer')
      .description('Get or set the target timer')
      .argument('timer', true, 'Optional target timer to set')
      .returns('timer', 'The current or set target timer')
      .done();

    builder.action('setTimer')
      .description('Set the target timer')
      .argument('timer', false, 'Target time to set')
      .returns('timer', 'The target time')
      .done();
  }

  timer(time) {
    if(typeof time === 'undefined') {
      return super.timer();
    }

    return this.setTimer(time);
  }

  setTimer(target) {
    try {
      target = duration(target);

      return Promise.resolve(this.changeTimer(target))
        .then(() => super.timer());
    } catch(ex) {
      return Promise.reject(ex);
    }
  }

  changeTimer(duration) {
    throw new Error('changeTimer not implemented');
  }

});
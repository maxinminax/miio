const Thing = require('abstract-things/thing');
const State = require('abstract-things/common/state');
const { duration } = require('abstract-things/values');

module.exports = Thing.mixin(Parent => class extends Parent.with(State) {

	static get capability() {
		return 'target-timeRemaining';
	}

	static availableAPI(builder) {
		builder.event('timeRemainingChanged')
			.type('timeRemaining')
			.description('The target timeRemaining has changed')
			.done();

		builder.action('timeRemaining')
			.description('Get the target timeRemaining')
			.returns('timeRemaining', 'The target timeRemaining')
			.done();
	}

	timeRemaining() {
		return Promise.resolve(this.getState('targetTemperature'));
	}

	updateTimeRemaining(target) {
		target = duration(target);

		if(this.updateState('timeRemaining', target)) {
			this.emitEvent('timeRemainingChanged', target);
		}
	}
});
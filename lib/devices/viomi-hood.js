const { Thing, Children } = require('abstract-things');
const { Light } = require('abstract-things/lights');

const MiioApi = require('../device');
const SubDevice = require('../sub-device');
const {
	Power,
	Status,
	ErrorState,
	SwitchablePower,
	SwitchableFanLevel,
} = require('./capabilities');

const FAN_LEVELS = [
	{
		id: '0',
		description: 'Off',
	},
	{
		id: '1',
		description: 'Low',
	},
	{
		id: '16',
		description: 'Middle',
	},
	{
		id: '4',
		description: 'High',
	},
	// 64: 'unknown'
];

const STATUSES = [
	{
		id: '0',
		description: 'Off',
	},
	{
		id: '1',
		description: 'Delay off',
	},
	{
		id: '2',
		description: 'On',
	},
	{
		id: '3',
		description: 'Cleaning',
	},
];

const ERRORS = [
	{
		id: '16',
		description: 'Communication error',
	},
	{
		id: '17',
		description: 'Push rod motor error',
	},
];

module.exports = Thing.type(
	(Parent) =>
		class extends Parent.with(
			MiioApi,
			Children,
			Power,
			Status,
			ErrorState,
			SwitchablePower,
			SwitchableFanLevel
		) {
			static get type() {
				return 'miio:viomi-hood';
			}

			constructor(options) {
				super(options);

				this.addChild(new HoodLight(this, 'light'));

				this.defineProperty('run_time', {
					name: 'runTime',
				});

				this.defineProperty('power_state', {
					name: 'status',
					mapper: String,
				});
				this.updateStatuses(STATUSES);

				this.defineProperty('wind_state', {
					name: 'fanLevel',
					mapper: String,
				});
				this.updateFanLevels(FAN_LEVELS);

				this.defineProperty('link_state', {
					name: 'link',
				});

				this.defineProperty('stove1_data', {
					name: 'stove1',
				});

				this.defineProperty('stove2_data', {
					name: 'stove2',
				});

				this.defineProperty('battary_life', {
					name: 'battaryLife',
				});

				this.defineProperty('run_status', {
					name: 'error',
					mapper: String,
				});
				this.updateErrors(ERRORS);

				this.defineProperty('poweroff_delaytime', {
					name: 'delayTime',
				});

				this.defineProperty('fixed_time', {
					name: 'fixedTime',
				});

				this.defineProperty('ble_slave_info', {
					name: 'ble',
					handler(result, value) {
						const info = value.split(',');
						result.bleModel = info[0];
						result.bleVersion = info[1];
						result.bleInfo = info;
					},
				});
			}

			propertyUpdated(key, value, oldValue) {
				if (key === 'status') {
					this.updatePower(value === '1' || value === '2');
				}
				super.propertyUpdated(key, value, oldValue);
			}

			changePower(power) {
				if (power) {
					return this.call('set_wind', [1], {
						refresh: ['fanLevel', 'state'],
					});
				}
				return this.call('set_power', [0], {
					refresh: ['fanLevel', 'state'],
				});
			}

			changeFanLevel(fanLevel) {
				let method, params;
				if (fanLevel === 'off') {
					method = 'set_power';
					params = [0];
				} else {
					method = 'set_wind';
					params = [parseInt(fanLevel)];
				}
				return this.call(method, params, {
					refresh: ['fanLevel', 'state'],
				});
			}
		}
);

class HoodLight extends Light.with(SubDevice, SwitchablePower) {
	constructor(...args) {
		super(...args);

		this.defineProperty('light_state', {
			name: 'power',
			mapper: Boolean,
		});
	}

	changePower(power) {
		const param = power ? 1 : 0;
		return this.parent.call('set_light', [param], {
			refresh: ['light:power'],
		});
	}
}

// power: 0 - off, 1 - delay off, 2 - on, 3 - cleaning, 4 - cleaning

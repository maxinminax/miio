const _ = require('lodash');
const { Children } = require('abstract-things');
const { Cooker } = require('abstract-things/appliances');
const MiioApi = require('../device');
const { AutonomousCooking } = require('./capabilities');

const CODES = {
	'0': 0,
	'1': 1,
	'2': 2,
	'3': 3,
	'4': 4,
	'5': 5,
	'6': 6,
	'7': 7,
	'8': 8,
	'9': 9,
	':': 10,
	';': 11,
	'<': 12,
	'=': 13,
	'>': 14,
	'?': 15,
	'@': 16,
	A: 17,
	B: 18,
	C: 19,
	D: 20,
	E: 21,
	F: 22,
	G: 23,
	H: 24,
	I: 25,
	J: 26,
	K: 27,
	L: 28,
	M: 29,
	N: 30,
	O: 31,
	P: 32,
	Q: 33,
	R: 34,
	S: 35,
	T: 36,
	U: 37,
	V: 38,
	W: 39,
	X: 40,
	Y: 41,
	Z: 42,
	'[': 43,
	'*': 44,
	']': 45,
	'^': 46,
	_: 47,
	'+': 48,
	a: 49,
	b: 50,
	c: 51,
	d: 52,
	e: 53,
	f: 54,
	g: 55,
	h: 56,
	i: 57,
	j: 58,
	k: 59,
	l: 60,
	m: 61,
	n: 62,
	o: 63,
	p: 64,
	q: 65,
	r: 66,
	s: 67,
	t: 68,
	u: 69,
	v: 70,
	w: 71,
	x: 72,
	y: 73,
	z: 74,
	'{': 75,
	'|': 76,
	'}': 77,
	'~': 78,
};

const MODES = [
	{
		id: '1',
		description: 'rice',
	},
	{
		id: '0',
		description: 'cooking',
	},
];

const STATUSES = [
	{
		id: '0',
		description: 'Standby',
	},
	{
		id: '1',
		description: 'Waiting',
	},
	{
		id: '2',
		description: 'Add rice',
	},
	{
		id: '3',
		description: 'Wash rice',
	},
	{
		id: '4',
		description: 'Cooking',
	},
	{
		id: '5',
		description: 'Water adding',
	},
	{
		id: '6',
		description: 'Small fire',
	},
	{
		id: '7',
		description: 'Boiling',
	},
	{
		id: '8',
		description: 'Braising',
	},
	{
		id: '9',
		description: 'Keeping warm',
	},
	{
		id: '10',
		description: 'Cleaning',
	},
	{
		id: '11',
		description: 'Disinfecting',
	},
	{
		id: '12',
		description: 'Box emptied',
	},
];

const ERRORS = {
	1: 'Product reset, please operate later',
	2: 'Please put it in the inner pot before operation',
	3: 'Please close the lid and then operate',
	4: 'Please clean up the food in the pan before you operate',
	5: 'You can add rice ingredients to the inner pot',
	6: 'The water is boiling and the ingredients can be added to the inner pot',
	7: 'Please replenish the rice in time',
	8: 'There was a power outage',
	9: 'Make sure the water is open and start again',
	10: 'Make sure the drain is blocked and start again',
	11: 'Cancel successfully, rice has been added to the pot, please reset within 30 minutes for additional amount, and if you need to reduce the amount, please clean the rice in the pan and start again',
	12: 'You can add ingredients to your baby',
	13: 'You can add porridge ingredients to the inner pot',
	14: 'Rice box empty complete',
	15: 'The rice tastes good for two hours',
};

const PALATES = {
	1: 'Porridge',
	2: 'Thick porridge',
	3: 'Baby porrridge',
	4: 'GD porrridge',
	6: 'Sweet cooking',
	8: 'Claypot rice',
	9: 'Quick boiling',
};

const SEEDS = {
	1: 'Northeast rice',
	2: 'Long grain rice',
	3: 'Thai rice',
	4: 'Daohuaxiang rice',
	5: 'Wuchang rice',
	6: 'Coarse Cereals',
};

const GRAINS = {
	1: 'Millet',
	2: 'Red bean',
	3: 'Black bean',
	4: 'Mung bean',
	5: 'Black rice',
	6: 'Red rice',
	7: 'Corn',
	8: 'Coix seed',
};

const COOKS = {
	1: 'Soup(water)',
	2: 'Steamed bun',
	3: 'Steamed dumplings',
	4: 'Steamed fish',
	5: 'Steamed pork ribs',
	6: 'Steamed sweet potato',
	7: 'Steamed corn',
	8: 'Rice dumpling(water)',
	9: 'Dumpling(water)',
	10: 'Noodles(water)',
	11: 'Rice noodles(water)',
	12: 'Make cakes',
	13: 'Reheat',
	14: 'Warm',
};

function codeToNum(str) {
	return _.get(CODES, str.toString(), 48);
}

function numberToCode(num) {
	return _.findKey(CODES, (i) => i.toString() === num.toString()) || '';
}

module.exports = class extends Cooker.with(MiioApi, Children, AutonomousCooking) {
	static get type() {
		return 'miio:cooker';
	}

	constructor(options) {
		super(options);

		this.defineProperty(0, {
			name: 'mode',
			mapper: String,
		});
		this.updateModes(MODES);

		this.defineProperty(1, {
			name: 'status',
			mapper: (v) => String(codeToNum(v)),
		});
		this.updateStatuses(STATUSES);

		this.defineProperty(2, {
			name: 'allow',
			mapper: Number,
		});

		this.defineProperty(3, {
			name: 'pHours',
			// mapper: Number
		});

		this.defineProperty(4, {
			name: 'pMinutes',
			// mapper: Number
		});

		this.defineProperty(5, {
			name: 'remainingRice',
			mapper: (v) => parseInt(codeToNum(v) * 5),
		});

		this.defineProperty(6, {
			name: 'error',
			mapper: (v) => {
				const num = codeToNum(v);
				return num === 0 ? null : String(num);
			},
		});
		this.updateErrors(ERRORS);

		this.defineProperty(7, {
			name: 'tHours',
			// mapper: Number
		});

		this.defineProperty(8, {
			name: 'tMinutes',
			// mapper: Number
		});

		this.defineProperty(9, {
			name: 'rHours',
			mapper: (v) => codeToNum(v),
		});

		this.defineProperty(10, {
			name: 'rMinutes',
			mapper: (v) => codeToNum(v),
			handler: (result, value) => {
				const hours = result.rHours;
				const minutes = codeToNum(value);
				result['rMinutes'] = minutes;
				result['remainingTime'] = hours + minutes * 60;
			},
		});

		this.defineProperty(11, {
			name: 'wHours',
			mapper: (v) => codeToNum(v),
		});

		this.defineProperty(12, {
			name: 'wMinutes',
			mapper: (v) => codeToNum(v),
			handler: (result, value) => {
				const hours = result.wHours;
				const minutes = codeToNum(value);
				result['wMinutes'] = minutes;
				result['warmingTime'] = hours + minutes * 60;
			},
		});

		this.defineProperty(13, {
			name: 'palate',
			mapper: (v) => _.get(PALATES, v, PALATES['other']),
		});

		this.defineProperty(14, {
			name: 'seed',
			mapper: (v) => SEEDS[v] || '',
		});

		this.defineProperty(15, {
			// number of cup 1-8
			name: 'size',
			mapper: Number,
		});

		this.defineProperty(16, {
			name: 'grain',
			mapper: (v) => GRAINS[v] || '',
		});

		this.defineProperty(17, {
			name: 'cup',
			mapper: (v) => parseInt(v) * 30,
		});

		this.defineProperty(18, {
			name: 'cook',
			mapper: (v) => COOKS[codeToNum(v)] || '',
		});

		this.defineProperty(19, {
			name: 'timeS',
			// mapper: Number
		});

		this.defineProperty(20, {
			name: 'error',
			mapper: (v) => {
				const code = codeToNum(v);
				if (code === 0) {
					return null;
				}
				return {
					code: code,
					message:
						'Device failure, fault code E' +
						code +
						', please refer to the fault guidance for troubleshooting or contact customer service for repair',
				};
			},
		});
	}

	loadProperties(props) {
		props = props.map((key) => this._reversePropertyDefinitions[key] || key);

		return this.call('get_props', []).then((result) => {
			const obj = {};
			for (let i = 0; i < props.length; i++) {
				this._pushProperty(obj, props[i], result[props[i]]);
			}
			return obj;
		});
	}

	cancel() {
		return this.call('Set_Off', [1]);
	}

	calibration() {
		return this.call('Set_Check', []);
	}

	empty() {
		return this.call('Set_Empty', [1]);
	}

	save() {
		// return this.call('Set_Save', [ricePorridge, porridge, rice, time, time, porridgeSize]);
	}

	sendRiceCode() {
		return this.call('Set_StartR', []);
	}

	sendCookCode() {
		return this.call('Set_StartC', []);
	}

	cancelRemind() {
		return this.call('Set_Remind', [1]);
  }
  
  activateCooking() {
    return this.mode()
      .then((mode) => {
        const method = mode.id === '1' ? 'Set_StartR' : 'Set_StartC';
        return this.call(method, []);
      });
  }

  deactivateCooking() {
    return this.call('Set_Off', [1]);
  }
};
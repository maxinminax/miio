const { Thing, Children } = require('abstract-things');
const { Light } = require('abstract-things/lights');
const { Dryer } = require('abstract-things/appliances');
const MiioApi = require('../device');
const SubDevice = require('../sub-device');
const {
	Status,
	SwitchablePower,
	SwitchableMode,
	SwitchableNightMode,
	TimeRemaining,
} = require('./capabilities');

const STATUSES = [
	{
		id: 'stop',
		description: 'Stop',
	},
	{
		id: 'down',
		description: 'Down',
	},
	{
		id: 'up',
		description: 'Up',
	},
];

module.exports = Thing.type(
	(Parent) =>
		class extends Parent.with(MiioApi, Children, Status, SwitchableNightMode) {
			static get type() {
				return 'miio:airer';
			}

			constructor(options) {
				super(options);

				this.addChild(new ArierLight(this, 'light'));
				this.addChild(new ArierDryer(this, 'dryer'));

				this.defineProperty('airer_state', {
					name: 'status',
				});
				this.updateStatuses(STATUSES);

				this.defineProperty('en_night_tip_light', {
					name: 'nightMode',
				});

				this.defineProperty('level', {
					name: 'level',
				});

				this.defineProperty('limit_configured', {
					name: 'limitConfigured',
				});

				this.defineProperty('limit_locked', {
					name: 'limitLocked',
				});

				this.defineProperty('run_time', {
					name: 'runTime',
				});
			}

			loadProperties(props) {
				props = props.map(
					(key) => this._reversePropertyDefinitions[key] || key
				);

				return this.call('get_prop', ['lumi.0', ...props]).then((result) => {
					const obj = {};
					for (let i = 0; i < result.length; i++) {
						this._pushProperty(obj, props[i], result[i]);
					}
					return obj;
				});
			}

			changeNightMode(nightMode) {
				return this.call(
					'set_device_prop',
					{
						sid: 'lumi.0',
						en_night_tip_light: nightMode ? 1 : 0,
					},
					{
						refresh: ['nightMode'],
					}
				);
			}

			setLight(light) {
				return this.child('light').setPower(light);
			}

			setDry(mode, remainingTime) {
				if (!mode) {
					return this.child('dryer').setPower(false);
				}
				return this.child('dryer').setMode(mode, remainingTime);
			}

			up() {
				return this.call('toggle_device', ['up'], {
					refresh: ['state'],
				});
			}

			down() {
				return this.call('toggle_device', ['down'], {
					refresh: ['state'],
				});
			}

			stop() {
				return this.call('toggle_device', ['stop'], {
					refresh: ['state'],
				});
			}
		}
);

class ArierLight extends Light.with(SubDevice, SwitchablePower) {
	constructor(parent, id) {
		super(parent, id);

		this.defineProperty('light', {
			name: 'power',
			mapper: (v) => v === 'on',
		});
	}

	changePower(power) {
		const param = power ? 'on' : 'off';
		return this.parent.call('toggle_light', [param], {
			refresh: ['light:power'],
		});
	}
}

class ArierDryer extends Dryer.with(
	SubDevice,
	SwitchablePower,
	SwitchableMode,
	TimeRemaining,
	Status
) {
	constructor(parent, id) {
		super(parent, id);

		this.defineProperty('dry_remaining_time', {
			name: 'timeRemaining',
		});

		this.defineProperty('dry_status', {
			name: 'status',
    });
    this.updateStatuses([
      {
        id: 'off',
        description: 'Off'
      },
			{
				id: 'winddry',
				description: 'Wind dry',
			},
			{
				id: 'hotdry',
				description: 'Hot dry',
			},
    ]);

		this.updateModes([
			{
				id: 'winddry',
				description: 'Wind dry',
			},
			{
				id: 'hotdry',
				description: 'Hot dry',
			},
		]);
	}

	propertyUpdated(key, value, oldValue) {
		if (key === 'status') {
			this.updatePower(value !== 'off');
			this.updateMode(value === 'off' ? null : value);
		}
	}

	changePower(power) {
    if (power) {
      return this.setMode('winddry', 1800);
    }
		return this.mode().then((mode) => {
			const method = `stop_${mode}`;
			return this.parent.call('control_device', [method], {
        refresh: ['dryer:status', 'dryer:timeRemaining'],
			});
		});
	}

	changeMode(mode, remainingTime) {
		const method = `start_${mode}`;
		return this.parent.call('control_device', [method, remainingTime], {
			refresh: ['dryer:status', 'dryer:timeRemaining'],
		});
	}
}

const _ = require('lodash');
const { Thing } = require('abstract-things');
const MiioApi = require('../device');
const {
	SwitchablePower,
	SwitchableMode,
	SwitchableWaterLevel,
	AdjustableTargetTemperature,
} = require('./capabilities');

const STATUSES = [
	{
		id: '0',
		description: 'Off'
	},
	{
		id: '2',
		description: 'Heat preservating'
	}
];

const MODES = [
	{
		id: '0',
		description: 'Thermostatic'
	},
	{
		id: '1',
		description: 'Heating'
	}
];

const WATER_LEVELS = [
	{
		id: '0',
		description: 'Single tube'
	},
	{
		id: '1',
		description: 'Dual tube'
	}
];

module.exports = Thing.type(
	(Parent) =>
		class extends Parent.with(
			MiioApi,
			SwitchablePower,
			SwitchableMode,
			SwitchableWaterLevel,
			AdjustableTargetTemperature
		) {
			static get type() {
				return 'miio:viomi-water-purifier';
			}

			constructor(options) {
				super(options);

				this.defineProperty('washStatus', {
					name: 'status',
					mapper: String
				});
				this.updateStatuses(STATUSES);

				this.defineProperty('velocity', {
					name: 'velocity',
				});

				this.defineProperty('waterTemp', {
					name: 'temperature',
				});

				this.defineProperty('targetTemp', {
					name: 'targetTemperature',
				});

				this.defineProperty('errStatus', {
					name: 'error',
				});

				this.defineProperty('hotWater', {
					name: 'remainingWater',
				});

				this.defineProperty('needClean', {
					name: 'needClean',
					mapper: Boolean,
				});

				this.defineProperty('rodUse', {
					name: 'rodUse',
				});

				this.defineProperty('rodLife', {
					name: 'rodLife',
				});

				this.defineProperty('yiJun', {
					name: 'yiJun',
				});

				this.defineProperty('modeType', {
					name: 'mode',
					mapper: String
				});
				this.updateModes(MODES);

				this.defineProperty('appointStart', {
					name: 'appointStart',
				});

				this.defineProperty('appointEnd', {
					name: 'appointEnd',
				});

				this.defineProperty('appointStatus', {
					name: 'appointStatus',
				});

				this.defineProperty('heatMode', {
					name: 'waterLevel',
					mapper: String
				});
				this.updateWaterLevels(WATER_LEVELS);
			}

			propertyUpdated(key, value, oldValue) {
				if (key === 'status') {
					this.updatePower(value !== 'off');
				}
				super.propertyUpdated(key, value, oldValue);
			}

			changePower(power) {
				const param = power ? 1 : 0;
				return this.call('set_power', [param], {
					refresh: ['status']
				});
			}

			changeMode(mode) {
				const param = parseInt(mode.id);
				return this.call('set_mode', [param], {
					refresh: ['status', 'mode']
				});
			}

			changeTargetTemperature(targetTemperature) {
				targetTemperature = targetTemperature.celsius;
				return this.call('set_temp', [targetTemperature], {
					refresh: ['targetTemperature']
				});
			}

			setWaterLevel(waterLevel) {
				const param = parseInt(waterLevel.id);
				return this.call('set_heat_mode', [param], {
					refresh: ['waterLevel']
				});
			}
		}
);

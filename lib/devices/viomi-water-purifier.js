const { WaterPurifier } = require('abstract-things/climate');
const MiioApi = require('../device');
const { RelativeTemperature, TDS } = require('./capabilities');

module.exports = class extends WaterPurifier.with(
	MiioApi,
  RelativeTemperature,
  TDS
) {
	static get type() {
		return 'miio:viomi-water-purifier';
	}

	constructor(options) {
		super(options);

		this.defineProperty('run_status', {
			name: 'status',
		});

		this.defineProperty('f1_totalflow', {
			name: 'f1TotalFlow',
		});

		this.defineProperty('f1_usedflow', {
			name: 'f1UsedFlow',
		});

		this.defineProperty('f1_totaltime', {
			name: 'f1TotalTime',
		});

		this.defineProperty('f1_usedtime', {
			name: 'f1UsedTime',
		});

		this.defineProperty('f2_totalflow', {
			name: 'f2TotalFlow',
		});

		this.defineProperty('f2_usedflow', {
			name: 'f2UsedFlow',
		});

		this.defineProperty('f2_totaltime', {
			name: 'f2TotalTime',
		});

		this.defineProperty('f2_usedtime', {
			name: 'f2UsedTime',
		});

		this.defineProperty('tds_in', {
			name: 'tdsIn',
		});

		this.defineProperty('tds_out', {
			name: 'tdsOut',
		});

		this.defineProperty('rinse', {
			name: 'rinse',
		});

		this.defineProperty('temperature', {
			name: 'relativeTemperature',
		});

		this.defineProperty('tds_warn_thd', {
			name: 'tdsWarnThd',
		});

		this.defineProperty('tds_out_avg', {
			name: 'tdsOutAvg',
		});
	}
};

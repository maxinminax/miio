const { Thing } = require('abstract-things');
const MiioApi = require('../device');
const { SwitchablePower, Status, WaterLevel } = require('./capabilities');

const STATUSES = [
	{
		id: 0,
		description: 'off',
	},
	{
		id: 1,
		description: 'Idle', // 'standby',
	},
	{
		id: 2,
		description: 'Delay', // 'appointments'
	},
	{
		id: 3,
		description: 'Washing', // 'cleaning'
	},
	{
		id: 4,
		description: 'Draining',
	},
	{
		id: 5,
		description: 'Inflowing', // 'info-water
	},
];

const TYPES = {
	0: 'fruit',
	1: 'seafood',
	2: 'meat',
};

const SUB_TYPES = {
	fruit: {
		0: 'vegetables',
		1: 'cabbage',
		2: 'lettuce',
		3: 'ipomoea-aquatica',
		4: 'cauliflower',
		5: 'chives',
		6: 'shallot',
		7: 'garlic',
		8: 'potato',
		9: 'taro',
		10: 'radishceltuce',
		11: 'yam',
		12: 'batata',
		13: 'loofah',
		14: 'cucumber',
		15: 'eggplant',
		16: 'benincasa-hispida',
		17: 'pumpkin',
		18: 'zucchini',
		19: 'tomato',
		20: 'bitter-gourd',
		21: 'chili',
		22: 'corn',
		23: 'edamame',
		24: 'pea',
		25: 'broad-bean',
		26: 'lentil',
		27: 'cowpea',
		28: 'green-beans',
		29: 'shiitake-mushroom',
		30: 'oyster-mushroom',
		31: 'enokitake',
		32: 'agaric',
		33: 'tremella',
		34: 'strawberry',
		35: 'apricot',
		36: 'plum',
		37: 'cherry',
		38: 'apple',
		39: 'pear',
		40: 'crataegus',
		41: 'grape',
	},
	seafood: {
		0: 'geoduck',
		1: 'large-abalone',
		2: 'small-abalone',
		3: 'trepang',
		4: 'testacean',
		5: 'shellfish',
	},
	meat: {
		0: 'pork',
		1: 'beef',
		2: 'lamb',
		3: 'dog',
		4: 'cony',
	},
};

const WATER_LEVELS = [
	{
		id: 0,
		description: 'Low',
	},
	{
		id: 1,
		description: 'Medium',
	},
	{
		id: 2,
		description: 'High',
	},
];

module.exports = Thing.type(
	(Parent) =>
		class extends Parent.with(MiioApi, SwitchablePower, Status, WaterLevel) {
			static get type() {
				return 'miio:fruit-purifier';
			}

			constructor(options) {
				super(options);

				this._props = [
					'state',
					'type',
					'subType',
					'unknown3',
					'level',
					'cleanTimeMinutes',
					'drainTimeMinutes',
					'delayTimeHours',
					'delayTimeMinutes',
					'unknown9',
				];

				this.defineProperty('state', {
					name: 'status',
				});
				this.updateStatuses(STATUSES);

				this.defineProperty('type', {
					mapper: (v) => {
						return TYPES[v];
					},
				});

				this.defineProperty('subType', {
					handler: (result, value) => {
						result['subType'] = SUB_TYPES[result.type]
							? SUB_TYPES[result.type][value]
							: null;
					},
				});

				this.defineProperty('level', {
					name: 'water',
				});
				this.updateWaterLevels(WATER_LEVELS);

				this.defineProperty('cleanTimeMinutes');

				this.defineProperty('drainTimeMinutes');

				this.defineProperty('delayTimeHours');

				this.defineProperty('delayTimeMinutes');
			}

			changePower(power) {
				const method = power ? 'set_open' : 'cancel_work';
				const params = power ? [1] : [];

				return this.call(method, params).then(MiioApi.checkOk);
			}

			loadProperties(props) {
				props = props.map(
					(key) => this._reversePropertyDefinitions[key] || key
				);

				return this.call('get_props', []).then((result) => {
					const obj = {};
					for (let i = 0; i < props.length; i++) {
						const prop = props[i];
						const j = this._props.indexOf(prop);
						this._pushProperty(obj, prop, j === -1 ? null : result[j]);
					}
					return obj;
				});
			}
		}
);

// [0, 0, 2, 0, 1, 0, 0, 0, 0, 0]
// [1, 0, 2, 0, 2, 0, 0, 0, 0, 0]
// [4, 1, 5, 0, 1, 0, 0, 0, 0, 0]
// [4, 1, 5, 0, 1, 0, 2, 0, 0, 1]
// ['mode', type, 'subType', '', 'level', 'cleanTimeMinutes', 'drainTimeMinutes', 'delayTimeHours', 'delayTimeMinutes', '']
// data	{"id":34,"method":"set_open","accessKey":"IOS00026747c5acafc2","params":[1]}
// data	{"id":61,"method":"set_drain","accessKey":"IOS00026747c5acafc2","params":[]} [4, 0, 2, 0, 1, 0, 2, 0, 0, 0] draining
// data	{"id":210,"method":"set_wash","accessKey":"IOS00026747c5acafc2","params":[1,0,0,1,1,1,2]} // hen gio
// data	{"id":464,"method":"get_error","accessKey":"IOS00026747c5acafc2","params":[]}

// data	{"id":2,"method":"get_error","accessKey":"IOS00026747c5acafc2","params":[]}
// data	{"id":15,"method":"set_open","accessKey":"IOS00026747c5acafc2","params":[1]}
// data	{"id":43,"method":"set_drain","accessKey":"IOS00026747c5acafc2","params":[]}
// data	{"id":52,"method":"cancel_work","accessKey":"IOS00026747c5acafc2","params":[]}
// data	{"id":62,"method":"set_wash","accessKey":"IOS00026747c5acafc2","params":[1,5,0,1,0,0,0]}

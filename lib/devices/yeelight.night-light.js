const { Thing } = require('abstract-things');
const MiioApi = require('../device');
const { SwitchableNightMode } = require('./capabilities');
const { Yeelight, ColorFull } = require('./yeelight');

const NightLight = Thing.mixin(
	(Parent) =>
		class extends Parent.with(MiioApi, SwitchableNightMode) {
			constructor(options) {
				super(options);

				this.defineProperty('nl_br', {
					name: 'nightLightBrightness',
					handler(result, value) {
						value = parseInt(value);
						if (value > 0) {
							result.brightness = value;
							result.nightMode = true;
						}
						else {
							result.nightMode = false;
						}
					}
				});
			}

			changeNightMode(nightMode) {
				const param = nightMode ? 'on' : 'off';
				return this.call('set_ps', ['nightlight', param], {
					refresh: ['power', 'nightLightBrightness'],
					refreshDelay: 200,
				});
			}

			changeBrightness(brightness, options) {
				if (brightness <= 0) {
					return this.changePower(false);
				} else {
					let promise;
					if (options.powerOn && this.power() === false) {
						// Currently not turned on
						promise = this.changePower(true);
					} else {
						promise = Promise.resolve();
					}

					return promise
						.then(() =>
							this.call(
								'set_bright',
								Yeelight.withEffect(brightness, options.duration),
								{
									refresh: ['brightness', 'mode'],
								}
							)
						);
				}
			}
		}
);

module.exports = class extends Yeelight.with(ColorFull, NightLight) {};

const { Thing, Children } = require('abstract-things');
const { Fan, Heater } = require('abstract-things/climate');
const { Light, ColorTemperature } = require('abstract-things/lights');
const { color } = require('abstract-things/values');
const MiotApi = require('../miot-device');
const SubDevice = require('../sub-device');
const {
	SwitchablePower,
	SwitchableMode,
	RelativeTemperature,
	RelativeHumidity,
	AdjustableTargetTemperature,
	Dimmable,
	Colorable,
	TimeRemaining,
} = require('./capabilities');

const spec = require('./specs/bath-heater.json');

module.exports = class extends Thing.with(
	MiotApi,
	Children,
	SwitchablePower,
	SwitchableMode,
	RelativeTemperature,
	RelativeHumidity
) {
	static get type() {
		return 'miot:viomi-bath-heater';
	}

	constructor(options) {
		super(options, spec);

		this.defineProperty('fan-control:indoor-temperature', {
			name: 'relativeTemperature',
		});

		this.defineProperty('fan-control:relative-humidity', {
			name: 'relativeHumidity',
		});
		this.defineProperty('fan-control:mode', {
			name: 'mode',
			mapper: String,
		});

		this.updateModes([
			{
				id: '0',
				description: 'None',
			},
			{
				id: '1',
				description: 'Bath',
			},
			{
				id: '2',
				description: 'Cool',
			},
			{
				id: '3',
				description: 'Dry',
			},
			{
				id: '4',
				description: 'Test',
			},
		]);

		this.defineProperty('fan-control:massage-manipulation', {
			name: 'power',
		});

		this.addChild(new BathLight(this, 'light'));
		this.addChild(new BathHeater(this, 'heater'));
		this.addChild(new BathFan(this, 'fan'));
		this.addChild(new BathReverseFan(this, 'reverse-fan'));
	}

	changeMode(mode) {
		const param = parseInt(mode.id);
		return this.changeProperty('mode', param);
	}

	changePower(power) {
		return this.changeProperty('power', power);
	}
};

class BathLight extends Light.with(
	SubDevice,
	SwitchablePower,
	SwitchableMode,
	Dimmable,
	Colorable,
	ColorTemperature
) {
	constructor(parent, id) {
		super(parent, id);

		this.defineProperty('light:on', {
			name: 'power',
		});

		this.defineProperty('light:mode', {
			name: 'mode',
		});
		this.updateModes([
			{
				id: true,
				description: 'Indicator light constantly on',
			},
			{
				id: false,
				description: 'Indicator light constantly off',
			},
		]);

		this.defineProperty('light:color-temperature', {
			name: 'colorTemperature',
		});
		this.updateColorTemperatureRange(2700, 6500);

		this.defineProperty('light:brightness', {
			name: 'brightness',
		});
	}

	propertyUpdated(key, value) {
		if (key === 'colorTemperature') {
			this.updateColor(color.temperature(value));
		}

		super.propertyUpdated(key, value);
	}

	changePower(power) {
		return this.parent.changeProperty('light:power', power);
	}

	changeMode(mode) {
		return this.parent.changeProperty('light:mode', mode);
	}

	changeColor(color) {
		return this.colorTemperatureRange().then((range) => {
			const colorTemperature = Math.min(
				Math.max(color.temperature.kelvins, range.min),
				range.max
			);
			return this.parent.changeProperty(
				'light:colorTemperature',
				colorTemperature
			);
		});
	}

	changeBrightness(brightness) {
		return this.parent.changeProperty('light:brightness', brightness);
	}
}

class BathHeater extends Heater.with(
	SubDevice,
	SwitchablePower,
	AdjustableTargetTemperature,
	TimeRemaining
) {
	constructor(parent, id) {
		super(parent, id);

		this.defineProperty('ptc-bath-heater:on', {
			name: 'power',
		});

		this.defineProperty('ptc-bath-heater:status', {
			name: 'status',
		});

		this.defineProperty('ptc-bath-heater:target-time', {
			name: 'timeRemaining',
		});

		this.defineProperty('ptc-bath-heater:target-temperature', {
			name: 'targetTemperature',
		});
		this.updateTargetTemperatureRange(25, 35);
	}

	changePower(power) {
		return this.parent.changeProperty('heater:power', power);
	}

	changeTargetTemperature(targetTemperature) {
		return this.targetTemperatureRange().then((range) => {
			const temperature = Math.min(
				Math.max(targetTemperature.celsius, range.min.celsius),
				range.max.celsius
			);
			return this.parent.changeProperty(
				'heater:targetTemperature',
				temperature
			);
		});
	}
}

class BathFan extends Fan.with(SubDevice, SwitchablePower) {
	constructor(parent, id) {
		super(parent, id);

		this.defineProperty('fan-control:motor-reverse', {
			name: 'motorReverse',
		});

		this.defineProperty('fan-control:horizontal-swing', {
			name: 'power',
		});
	}

	changePower(power) {
		return this.parent.changeProperty('fan:power', power);
	}
}

class BathReverseFan extends Fan.with(SubDevice, SwitchablePower) {
	constructor(parent, id) {
		super(parent, id);

		this.defineProperty('fan-control:motor-reverse', {
			name: 'power',
			mapper: (v) => {
				return v;
			},
		});
	}

	changePower(power) {
		return this.parent.changeProperty('reverse-fan:power', power);
	}
}

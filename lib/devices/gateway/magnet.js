const SubDevice = require('./subdevice');
const { ContactDetection } = require('../capabilities');
const Voltage = require('./voltage');

/**
 * Magnet device, emits events `open` and `close` if the state changes.
 */
module.exports = class Magnet extends SubDevice.with(ContactDetection, Voltage) {
	constructor(parent, info) {
		super(parent, info);

		this.miioModel = 'lumi.magnet';

		this.defineProperty('status');
	}

	_report(data) {
		if (data.no_close) {
			data.status = 'open';
		}
		super._report(data);
	}

	propertyUpdated(key, value, oldValue) {
		if(key === 'status') {
			// Change the contactDetection state
			const isContactDetection = value === 'close';
			this.updateContactDetected(isContactDetection);
		}

		super.propertyUpdated(key, value, oldValue);
	}
};

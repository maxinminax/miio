const SubDevice = require('./subdevice');
const { MotionDetection } = require('../capabilities');
const Voltage = require('./voltage');

/**
 * Motion detection sensing device, emits the event `motionDetection` whenever motion is detected.
 */
module.exports = class extends SubDevice.with(MotionDetection, Voltage) {
	constructor(parent, info) {
		super(parent, info);

		this.miioModel = 'lumi.motion';

		this.defineProperty('status');

		this.updateMotionDetected(false);
	}

	_report(data) {
		if (data.no_motion) {
			data.status = 'immobility';
		}
		super._report(data);
	}

	propertyUpdated(key, value, oldValue) {
		if(key === 'status') {
			const isMotionDetection = value === 'motion';
			this.updateMotionDetected(isMotionDetection);
		}

		super.propertyUpdated(key, value, oldValue);
	}
};

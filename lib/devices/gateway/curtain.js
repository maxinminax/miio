const SubDevice = require('./subdevice');
const { Polling } = require('abstract-things');
const { BatteryLevel, AdjustablePosition } = require('../capabilities');

module.exports = class extends SubDevice.with(
	Polling,
	BatteryLevel,
	AdjustablePosition
) {
	constructor(parent, info) {
		super(parent, info);

		this.miioModel = 'lumi.curtain';

		this.defineProperty('curtain_level', {
			name: 'position',
			mapper: Number,
		});

		this.defineProperty('battery', {
			name: 'batteryLevel',
			mapper: Number,
		});

		this.defineProperty('power_mode', {
			name: 'powerMode',
		});

		this.defineProperty('pos_limit', {
			name: 'positionLimit',
		});

		this.defineProperty('polarity', {
			name: 'polarity',
		});

		this.defineProperty('manual_enabled', {
			name: 'manualEnabled',
		});

		this.defineProperty('run_time', {
			name: 'runTime',
		});
	}

	_report(data) {
		super._report(data);
	}

	poll() {
		this.loadApiProperties();
	}

	_loadApiProperties() {
		return this._parent.call('get_device_prop', [
			`lumi.${this.internalId}`,
			...this._propertiesToMonitor,
		]);
	}

	adjustMaxPosition() {
		return this.call('toggle_device', ['open']);
		// return this._parent.devApi.write(this.internalId, [
		// 	{ curtain_status: 'open' },
		// ]);
	}

	adjustMinPosition() {
		return this.call('toggle_device', ['close']);
		// return this._parent.devApi.write(this.internalId, [
		// 	{ curtain_status: 'close' },
		// ]);
	}

	stopAdjustPosition() {
		return this.call('toggle_device', ['stop']);
	}

	changePosition(position) {
		return this.call('ctrl_device_prop', { curtain_level: position });
		// return this._parent.devApi.write(this.internalId, [
		// 	{ curtain_level: position },
		// ]);
	}
};

const SubDevice = require('./subdevice');
const {
	RelativeTemperature,
	RelativeHumidity,
} = require('../capabilities');
const Voltage = require('./voltage');

module.exports = class SensorHT extends SubDevice.with(
	RelativeTemperature,
	RelativeHumidity,
	Voltage
) {
	constructor(parent, info) {
		super(parent, info);

		this.miioModel = 'lumi.sensor_ht';

		this.defineProperty('temperature', {
			name: 'relativeTemperature',
			mapper: (v) => v / 100.0,
		});

		this.defineProperty('humidity', {
			name: 'relativeHumidity',
			mapper: (v) => v / 100.0,
		});
	}
};

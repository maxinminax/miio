const SubDevice = require('./subdevice');
const {
	RelativeTemperature,
	RelativeHumidity,
	AtmosphericPressure,
} = require('../capabilities');
const Voltage = require('./voltage');

module.exports = class WeatherSensor extends SubDevice.with(
	RelativeTemperature,
	RelativeHumidity,
	AtmosphericPressure,
	Voltage
) {
	constructor(parent, info) {
		super(parent, info);

		this.miioModel = 'lumi.weather';

		this.defineProperty('temperature', {
			name: 'relativeTemperature',
			mapper: (v) => v / 100.0,
		});

		this.defineProperty('humidity', {
			name: 'relativeHumidity',
			mapper: (v) => v / 100.0,
		});

		this.defineProperty('pressure', {
			name: 'atmosphericPressure',
		});
	}
};

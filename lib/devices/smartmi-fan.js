const { Fan } = require('abstract-things/climate');
const { angle } = require('abstract-things/values');

const MiioApi = require('../device');
const {
	SwitchablePower,
	SwitchableNightMode,
	AdjustableFanSpeed,
	SwitchableVerticalSwing,
	AdjustableTimer,
	SwitchableAudioFeedback,
	SwitchableLedLevel,
} = require('./capabilities');

const LED_LEVELS = [
	{
		id: '0',
		description: 'Bright',
	},
	{
		id: '1',
		description: 'Dim',
	},
	{
		id: '2',
		description: 'Off',
	},
];

module.exports = class extends Fan.with(
	MiioApi,
	SwitchablePower,
	SwitchableNightMode,
	AdjustableFanSpeed,
	SwitchableVerticalSwing,
	AdjustableTimer,
	SwitchableAudioFeedback,
	SwitchableLedLevel
) {
	static get type() {
		return 'miio:fan';
	}

	constructor(options) {
		super(options);

		this.defineProperty('power', {
			mapper: (v) => v === 'on',
		});

		this.defineProperty('angle', {
			name: 'verticalSwingAngle'
		});

		this.defineProperty('angle_enable', {
			name: 'verticalSwing',
			mapper: (v) => v === 'on',
		});

		this.defineProperty('speed_level', {
			name: 'fanSpeed',
			mapper: Number,
		});

		this.defineProperty('natural_level', {
			name: 'nightMode',
			handler(result, value) {
				value = parseInt(value);
				if (value > 0) {
					result.fanSpeed = value;
					result.nightMode = true;
				} else {
					result.nightMode = false;
				}
			},
		});

		this.defineProperty('child_lock', {
			name: 'childLock',
			mapper: Boolean,
		});

		this.defineProperty('poweroff_time', {
			name: 'timer',
			mapper: (v) => parseInt(v / 60),
		});

		this.defineProperty('buzzer', {
			name: 'audioFeedback',
			mapper: Boolean,
		});

		this.defineProperty('led_b', {
			name: 'ledLevel',
			mapper: String,
		});
		this.updateLedLevels(LED_LEVELS);
	}

	changePower(power) {
		return this.call('set_power', [power ? 'on' : 'off'], {
			refresh: ['power', 'fanSpeed', 'nightMode'],
		});
	}

	changeNightMode(nightMode) {
		const method = nightMode ? 'set_natural_level' : 'set_speed_level';
		return this.fanSpeed().then((fanSpeed) => {
			return this.call(method, [fanSpeed], {
				refresh: ['power', 'fanSpeed', 'nightMode'],
			});
		});
	}

	changeFanSpeed(fanSpeed) {
		return this.nightMode().then((nightMode) => {
			const method = nightMode ? 'set_natural_level' : 'set_speed_level';
			return this.call(method, [fanSpeed], {
				refresh: ['power', 'fanSpeed', 'nightMode'],
			});
		});
	}

	changeLedLevel(ledLevel) {
		const param = parseInt(ledLevel);
		return this.call('set_led_b', [param], { refresh: ['ledLevel'] });
	}

	changeAudioFeedback(audioFeedback) {
		const param = audioFeedback ? 1 : 0;
		return this.call('set_buzzer', [param], {
			refresh: ['audioFeedback'],
		});
	}

	changeVerticalSwing(verticalSwing) {
		const param = verticalSwing ? 'on' : 'off';
		return this.call('set_angle_enable', param, {
			refresh: ['verticalSwing', 'verticalSwingAngle'],
		});
	}

	setVerticalSwingAngle(angleValue) {
		const method = angleValue === 0 ? 'set_angle_enable' : 'set_angle';
		const params = angleValue === 0 ? ['off'] : [angleValue];
		return this.call(method, params, {
			refresh: ['verticalSwing', 'verticalSwingAngle'],
		});
	}

	setTimer(timer) {
		const param = timer * 60;
		return this.call('set_poweroff_time', [param], {
			refresh: ['power', 'timer'],
		});
	}
};

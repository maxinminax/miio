const _ = require('lodash');
const { AirPurifier } = require('abstract-things/climate');
const MiotApi = require('../miot-device');
const {
	ErrorState,
	SwitchablePower,
	SwitchableMode,
	SwitchableFanLevel,
	SwitchableLedLevel,
	SwitchableLed,
	SwitchableAudioFeedback,
	RelativeTemperature,
	RelativeHumidity,
	PM2_5,
} = require('./capabilities');

const spec = require('./specs/air-purifier.json');

const ERRORS = [
	{
		id: '0',
		description: 'no faults',
	},
	{
		id: '1',
		description: 'm1 run',
	},
	{
		id: '2',
		description: 'm1 stuck',
	},
	{
		id: '3',
		description: 'no sensor',
	},
	{
		id: '4',
		description: 'error hum',
	},
	{
		id: '5',
		description: 'error temp',
	},
];

const MODES = [
	{
		id: '0',
		description: 'Auto',
	},
	{
		id: '1',
		description: 'Sleep',
	},
	{
		id: '2',
		description: 'Favorite',
	},
	{
		id: '3',
		description: 'None',
	},
];

const FAN_LEVELS = [
	{
		id: '1',
		description: 'Level 1',
	},
	{
		id: '2',
		description: 'Level 2',
	},
	{
		id: '3',
		description: 'Level 3',
	},
];

const LED_LEVELS = [
	{
		id: '0',
		description: 'Bright',
	},
	{
		id: '1',
		description: 'Dim',
	},
	{
		id: '2',
		description: 'Off',
	},
];

module.exports = class extends AirPurifier.with(
	MiotApi,
	ErrorState,
	SwitchablePower,
	SwitchableMode,
	SwitchableFanLevel,
	RelativeTemperature,
	RelativeHumidity,
	PM2_5,
	SwitchableLed,
	SwitchableLedLevel,
	SwitchableAudioFeedback
) {
	static get type() {
		return 'miot:air-purifier';
	}

	constructor(options) {
		super(options, spec);

		this.defineProperty('air-purifier:fault', {
			name: 'error',
			mapper: (v) => (v === 0 ? null : v),
		});
		this.updateErrors(ERRORS);

		this.defineProperty('air-purifier:on', {
			name: 'power',
		});

		this.defineProperty('air-purifier:fan-level', {
			name: 'fanLevel',
			mapper: String,
		});
		this.updateFanLevels(FAN_LEVELS);

		this.defineProperty('air-purifier:mode', {
			name: 'mode',
			mapper: String,
		});
		this.updateModes(MODES);

		this.defineProperty('environment:pm2.5-density', {
			name: 'pm2_5',
			mapper: Number,
		});

		this.defineProperty('environment:relative-humidity', {
			name: 'relativeHumidity',
			mapper: Number,
		});

		this.defineProperty('environment:temperature', {
			name: 'relativeTemperature',
			mapper: Number,
		});

		this.defineProperty('filter:filter-life-level', {
			name: 'filterLifeRemaining',
		});

		this.defineProperty('filter:filter-used-time', {
			name: 'filterHoursUsed',
		});

		this.defineProperty('use-time:use-time', {
			name: 'useTime',
		});

		this.defineProperty('indicator-light:brightness', {
			name: 'ledLevel',
			mapper: String,
		});
		this.updateLedLevels(LED_LEVELS);

		this.defineProperty('indicator-light:on', {
			name: 'led',
		});

		this.defineProperty('alarm:alarm', {
			name: 'audioFeedback',
		});
	}

	changePower(power) {
		return this.changeProperty('power', power, {
			refresh: ['power', 'mode', 'fanLevel'],
		});
	}

	changeMode(mode) {
		const param = parseInt(mode.id);
		return this.changeProperty('mode', param, {
			refresh: ['power', 'mode', 'fanLevel'],
		});
	}

	changeFanLevel(fanLevel) {
		const param = parseInt(fanLevel.id);
		return this.changeProperty('fanLevel', param, {
			refresh: ['power', 'mode', 'fanLevel'],
		});
	}

	changeLedLevel(ledLevel) {
		const param = parseInt(ledLevel.id);
		return this.changeProperty('ledLevel', param);
	}

	changeLed(led) {
		return this.changeProperty('led', led);
	}

	changeAudioFeedback(audioFeedback) {
		return this.changeProperty('audioFeedback', audioFeedback);
	}
};

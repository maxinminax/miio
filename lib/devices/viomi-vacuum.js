const { Vacuum } = require('abstract-things/climate');
const MiioApi = require('../device');
const {
	ErrorState,
	Status,
	SwitchableMode,
	SwitchableFanLevel,
	SwitchableWaterLevel,
	BatteryLevel,
	SwitchableLed,
	AutonomousCleaning,
	SpotCleaning,
	AutonomousCharging,
	Muteable,
	AdjustableVolume,
	SwitchableLanguage
} = require('./capabilities');

const ERRORS = [
	{
		id: '500',
		description: 'Radar timed out',
	},
	{
		id: '501',
		description: 'Wheels stuck',
	},
	{
		id: '502',
		description: 'Low battery',
	},
	{
		id: '503',
		description: 'Dust bin missing',
	},
	{
		id: '508',
		description: 'Uneven ground',
	},
	{
		id: '509',
		description: 'Cliff sensor error',
	},
	{
		id: '510',
		description: 'Collision sensor error',
	},
	{
		id: '511',
		description: 'Could not return to dock',
	},
	{
		id: '512',
		description: 'Could not return to dock',
	},
	{
		id: '513',
		description: 'Could not navigate',
	},
	{
		id: '514',
		description: 'Vacuum stuck',
	},
	{
		id: '515',
		description: 'Charging error',
	},
	{
		id: '516',
		description: 'Mop temperature error',
	},
	{
		id: '521',
		description: 'Water tank is not installed',
	},
	{
		id: '522',
		description: 'Mop is not installed',
	},
	{
		id: '525',
		description: 'Insufficient water in water tank',
	},
	{
		id: '527',
		description: 'Remove mop',
	},
	{
		id: '528',
		description: 'Dust bin missing',
	},
	{
		id: '529',
		description: 'Mop and water tank missing',
	},
	{
		id: '530',
		description: 'Mop and water tank missing',
	},
	{
		id: '531',
		description: 'Water tank is not installed',
	},
	{
		id: '2101',
		description: 'Unsufficient battery, continuing cleaning after recharge',
	},
];

const STATUSES = [
	{
		id: '0',
		description: 'Idle not docked',
	},
	{
		id: '1',
		description: 'Idle',
	},
	{
		id: '2',
		description: 'Pause',
	},
	{
		id: '3',
		description: 'Cleaning',
	},
	{
		id: '4',
		description: 'Returning',
	},
	{
		id: '5',
		description: 'Docked',
	},
	{
		id: '6',
		description: 'Cleaning and mopping',
	},
	{
		id: '7',
		description: 'Mopping',
	},
];

const CLEANING_STATUSES = ['3', '6', '7'];
const CHARGING_STATUSES = ['4', '5'];

const MODES = [
	{
		id: '0',
		description: 'Vacuum',
	},
	{
		id: '1',
		description: 'Vacuum and mop',
	},
	{
		id: '2',
		description: 'Mop',
	},
];

const WATER_LEVELS = [
	{
		id: '11',
		description: 'Low',
	},
	{
		id: '12',
		description: 'Medium',
	},
	{
		id: '13',
		description: 'High',
	},
];

const FAN_LEVELS = [
	{
		id: '0',
		description: 'Silent',
	},
	{
		id: '1',
		description: 'Standard',
	},
	{
		id: '2',
		description: 'Medium',
	},
	{
		id: '3',
		description: 'Turbo',
	},
];

const BOX_TYPES = {
	1: 'vacuum',
	2: 'water',
	3: 'vacumm-and-water',
};

const LANGUAGUES = [
	{
		id: '1',
		language: 'China'
	},
	{
		id: '2',
		language: 'English'
	}
];

module.exports = class extends Vacuum.with(
	MiioApi,
	Status,
	ErrorState,
	SwitchableMode,
	SwitchableFanLevel,
	SwitchableWaterLevel,
	BatteryLevel,
	SwitchableLed,
	AutonomousCharging,
	AutonomousCleaning,
	SpotCleaning,
	AdjustableVolume,
	Muteable,
	SwitchableLanguage
) {
	static get type() {
		return 'miio:viomi-vacuum';
	}

	constructor(options) {
		super(options);

		this.defineProperty('run_state', {
			name: 'status',
			mapper: String,
		});
		this.updateStatuses(STATUSES);

		this.defineProperty('mode', {
			name: 'umode',
		});

		this.defineProperty('err_state', {
			name: 'error',
			mapper: (v) => (v === 0 ? null : String(v)),
		});
		this.updateErrors(ERRORS);

		this.defineProperty('battary_life', {
			name: 'batteryLevel',
		});

		this.defineProperty('box_type', {
			name: 'boxType',
			mapper: (v) => {
				return BOX_TYPES[v];
			},
		});

		this.defineProperty('mop_type', {
			name: 'mopType',
		});

		this.defineProperty('s_time', {
			name: 'cleanTime',
		});

		this.defineProperty('s_area', {
			name: 'cleanArea',
		});

		this.defineProperty('suction_grade', {
			name: 'fanLevel',
			mapper: String,
		});
		this.updateFanLevels(FAN_LEVELS);

		this.defineProperty('water_grade', {
			name: 'waterLevel',
			mapper: String,
		});
		this.updateWaterLevels(WATER_LEVELS);

		this.defineProperty('remember_map', {
			name: 'rememberMap',
		});

		this.defineProperty('has_map', {
			name: 'hasMap',
		});

		this.defineProperty('has_newmap', {
			name: 'hasNewMap',
		});

		this.defineProperty('is_mop', {
			name: 'mode',
			mapper: String
		});
		this.updateModes(MODES);

		this.defineProperty('mop_route', {
			name: 'mopRoute',
		});

		this.defineProperty('map_num', {
			name: 'mapNum',
		});

		this.defineProperty('map_id', {
			name: 'mapId',
		});

		this.defineProperty('repeat_state', {
			name: 'repeatState',
		});

		this.defineProperty('v_state', {
			name: 'volume',
			mapper: (v) => v * 10,
		});

		this.defineProperty('side_brush_hours', {
			name: 'sideBrushHours',
		});

		this.defineProperty('main_brush_hours', {
			name: 'mainBrushHours',
		});

		this.defineProperty('hypa_hours', {
			name: 'hypaHours',
		});

		this.defineProperty('mop_hours', {
			name: 'mopHours',
		});

		this.defineProperty('side_brush_life', {
			name: 'sideBrushLife',
		});

		this.defineProperty('main_brush_life', {
			name: 'mainBrushLife',
		});

		this.defineProperty('hypa_life', {
			name: 'hypaLife',
		});

		this.defineProperty('mop_life', {
			name: 'mopLife',
		});
		
		this.updateLanguages(LANGUAGUES);
	}

	propertyUpdated(key, value, oldValue) {
		if (key === 'status') {
			this.updateCharging(CHARGING_STATUSES.indexOf(value) > -1);
			this.updateCleaning(CLEANING_STATUSES.indexOf(value) > -1);
		}
		super.propertyUpdated(key, value, oldValue);
	}

	changeMode(mode) {
		const param = parseInt(mode.id);
		return this.call('set_mop', [param], {
			refresh: ['mode'],
		});
	}

	activateCharging() {
		return this.call('set_charge', [1], {
			refresh: ['status'],
		});
	}

	deactivateCharging() {
		return this.call('set_charge', [0], {
			refresh: ['status'],
		});
	}

	uncharge() {
		return this.deactivateCharging();
	}

	activateCleaning() {
		return this.call('set_mode_withroom', [0, 1, 0], {
			refresh: ['status'],
		});
	}

	deactivateCleaning() {
		return this.call('set_mode', [0], {
			refresh: ['status'],
		});
	}

	pause() {
		return this.call('set_mode_withroom', [0, 2, 0], {
			refresh: ['status'],
		});
	}

	changeFanLevel(fanLevel) {
		const param = parseInt(fanLevel.id);
		return this.call('set_suction', [param], {
			refresh: ['fanLevel'],
		});
	}

	changeWaterLevel(waterLevel) {
		const param = parseInt(waterLevel.id);
		return this.call('set_suction', [param], {
			refresh: ['waterLevel'],
		});
	}

	changeMuted(muted) {
		const param = muted ? 0 : 1;
		return this.call('set_voice', [param, 5], {
			refresh: ['volume'],
		});
	}

	changeVolume(volume) {
		const param = parseInt(volume / 10);
		return this.call('set_voice', [1, param], {
			refresh: ['volume'],
		});
	}

	changeLed(led) {
		const param = led ? 'on' : 'off';
		return this.call('set_light', [param], {
			refresh: ['led'],
		});
	}

	changeLanguage(language) {
		const param = parseInt(language);
		return this.call('set_language', [param]);
	}

	find() {
		return this.call('set_resetpos', [1]);
	}
};
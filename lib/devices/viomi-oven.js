const _ = require('lodash');
const { Thing, Children } = require('abstract-things');
const {
	RelativeTemperature,
	ContactDetection,
} = require('abstract-things/sensors');

const MiioApi = require('../device');
const SubDevice = require('../sub-device');
const {
	Status,
	ErrorState,
	SwitchableMode,
	SwitchablePower,
	AdjustableTargetTemperature,
	AdjustableTimer,
	TimeRemaining,
} = require('./capabilities');

const STATUSES = [
	{
		id: '0',
		description: 'Idle',
	},
	{
		id: '1',
		description: 'Cooking',
	},
	{
		id: '2',
		description: 'Pausing',
	},
	{
		id: '3',
		description: 'Reserved',
	},
	{
		id: '4',
		description: 'Finished',
	},
];

const MODES = [
	{
		id: '0',
		description: 'Quick steaming'
	},
	{
		id: '1',
		description: 'High temperature steaming'
	},
	{
		id: '2',
		description: 'Buns'
	},
	{
		id: '3',
		description: 'Rice'
	},
	{
		id: '4',
		description: 'Steamed vermicelli roll'
	},
	{
		id: '5',
		description: 'Water egg'
	},
	{
		id: '6',
		description: 'Fish'
	},
	{
		id: '7',
		description: 'Meat'
	},
	{
		id: '8',
		description: 'PorkChop'
	},
	{
		id: '9',
		description: 'Steamed pork with rice flour'
	},
	{
		id: '10',
		description: 'Shrimp crab'
	},
	{
		id: '11',
		description: 'Vegetable'
	},
	{
		id: '12',
		description: 'Potato'
	},
	{
		id: '13',
		description: 'Fermenting'
	},
	{
		id: '14',
		description: 'Soup stewing'
	},
	{
		id: '15',
		description: 'Defreezing'
	},
	{
		id: '16',
		description: 'Preheating'
	},
	{
		id: '17',
		description: 'Hot air baking'
	},
	{
		id: '18',
		description: 'Pizza baking'
	},
	{
		id: '19',
		description: 'Strong grill'
	},
	{
		id: '20',
		description: 'Grill'
	},
	{
		id: '21',
		description: 'Fan baking'
	},
	{
		id: '22',
		description: 'Cluster baking'
	},
	{
		id: '23',
		description: 'Slow baking'
	},
	{
		id: '24',
		description: 'Crisp baking'
	},
	{
		id: '25',
		description: 'Tendering baking',
	}
];

const ERRORS = [
	{
		id: '1',
		description: 'Temperature sensor in the case short circuit'
	},
	{
		id: '2',
		description: 'Temperature sensor in the case open circuit'
	},
	{
		id: '3',
		description: 'Heating plate temperature sensor short circuit'
	},
	{
		id: '4',
		description: 'Heating plate temperature sensor open circuit'
	},
	{
		id: '5',
		description: 'Display panel and power board telecommunication error'
	},
	{
		id: '6',
		description: 'Liner not reaching the setting temperature'
	},
	{
		id: '7',
		description: 'Temperature sensor in the case with fault',
	}
];

module.exports = class extends Thing.with(
	MiioApi,
	Children,
	Status,
	ErrorState,
	SwitchablePower,
	SwitchableMode,
	RelativeTemperature,
	ContactDetection,
	TimeRemaining
) {
	static get type() {
		return 'miio:viomi-oven';
	}

	constructor(options) {
		super(options);

		this.defineProperty('hwInfo', {
			name: 'hwInfo',
		});

		this.defineProperty('swInfo', {
			name: 'swInfo',
		});

		this.defineProperty('error', {
			name: 'error',
			mapper: String
		});
		this.updateErrors(ERRORS);

		this.defineProperty('dishId', {
			name: 'dishId',
		});

		this.defineProperty('dishName', {
			name: 'dishName',
		});

		this.defineProperty('status', {
			name: 'status',
			mapper: String
		});
		this.updateStatuses(STATUSES);

		this.defineProperty('mode', {
			name: 'mode',
			mapper: (v) => v === '' ? null : v
		});
		this.updateModes(MODES);

		this.defineProperty('workTime', {
			name: 'workTime',
		});

		this.defineProperty('temp', {
			name: 'relativeTemperature',
		});

		this.defineProperty('leftTime', {
			name: 'timeRemaining',
		});

		this.defineProperty('waterTank', {
			name: 'waterTank',
			mapper: Boolean,
		});

		this.defineProperty('prepareTime', {
			name: 'prepareTime',
		});

		this.defineProperty('doorIsOpen', {
			name: 'contactDetected',
			mapper: Boolean,
		});

		this.addChild(new Steaming(this, 'steaming'));
		this.addChild(new Baking(this, 'baking'));
	}

	propertyUpdated(key, value, oldValue) {
		if (key === 'status') {
			this.updatePower(value !== '0');
		}
		super.propertyUpdated(key, value, oldValue);
	}

	prepare(mode, tempSetZ, timeSetZ, tempSetK, timeSetK, orderFinishTime) {
		return this.call(
			'setPrepareMode',
			[mode, tempSetZ, timeSetZ, tempSetK, timeSetK, orderFinishTime],
			{
				refresh: ['state', 'mode'],
			}
		);
	}

	start(mode, tempSetZ, timeSetZ, tempSetK, timeSetK) {
		return this.call(
			'setStartMode',
			[mode, tempSetZ, timeSetZ, tempSetK, timeSetK],
			{
				refresh: ['state', 'mode'],
			}
		);
	}

	pause() {
		return this.call('setPause', [1], {
			refresh: ['status'],
		});
	}

	resume() {
		return this.call('setPause', [0], {
			refresh: ['status'],
		});
	}

	end() {
		return this.call('setEnd', [], {
			refresh: ['status'],
		});
	}

	changePower(power) {
		return this.call(power ? 'setBootUp' : 'setTurnOff', [], {
			refresh: ['mode', 'status'],
		});
	}
};

class Steaming extends Thing.with(
	SubDevice,
	AdjustableTargetTemperature,
	AdjustableTimer
) {
	constructor(...args) {
		super(...args);

		this.defineProperty('tempSetZ', {
			name: 'targetTemperature',
		});

		this.defineProperty('timeSetZ', {
			name: 'timer',
		});
	}
}

class Baking extends Thing.with(
	SubDevice, AdjustableTargetTemperature, AdjustableTimer) {
	constructor(...args) {
		super(...args);

		this.defineProperty('tempSetK', {
			name: 'targetTemperature',
		});

		this.defineProperty('timeSetK', {
			name: 'timer',
		});
	}
}

const { Thing } = require('abstract-things');
const MiioApi = require('../device');
const {
	Status,
	SwitchableMode,
	AutonomousCleaning,
	RelativeTemperature,
	TimeRemaining,
} = require('./capabilities');

const MODES = [
	{
		id: '0',
		description: 'Standard',
	},
	{
		id: '1',
		description: 'Saver',
	},
	{
		id: '2',
		description: 'Fast',
	},
	// {
	// 	id: '3',
	// 	description: 'Custom',
	// },
	{
		id: '3|3',
		description: 'Custom - Strong'
	},
	{
		id: '3|4',
		description: 'Custom - Prepare'
	},
	{
		id: '3|5',
		description: 'Custom - Glass'
	},
	{
		id: '3|6',
		description: 'Custom - Degerming'
	},
	{
		id: '3|250',
		description: 'Custom - Dry'
	}
];

const STATUSES = [
	{
		id: '0',
		description: 'Free',
	},
	{
		id: '1',
		description: 'Prewash',
	},
	{
		id: '2',
		description: 'Mainwash',
	},
	{
		id: '3',
		description: 'Reclean',
	},
	{
		id: '4',
		description: 'Swash',
	},
	{
		id: '5',
		description: 'Dry',
	},
	{
		id: '6',
		description: 'Finished',
	},
];

module.exports = Thing.type(
	(Parent) =>
		class extends Parent.with(
			MiioApi,
			Status,
			SwitchableMode,
			AutonomousCleaning,
			RelativeTemperature,
			TimeRemaining
		) {
			static get type() {
				return 'miio:viomi-dishwasher';
			}

			constructor(options) {
				super(options);

				this.updateModes(MODES);

				this.defineProperty('child_lock', {
					name: 'childLock',
					mapper: Boolean
				});

				this.defineProperty('program', {
					name: 'program',
					mapper: String,
				});

				this.defineProperty('wash_status', {
					name: 'cleaning',
					mapper: Boolean,
				});

				this.defineProperty('wash_process', {
					name: 'status',
					mapper: String,
				});
				this.updateStatuses(STATUSES);

				this.defineProperty('ldj_state', {
					name: 'ldjState',
				});

				this.defineProperty('salt_state', {
					name: 'saltState',
				});

				this.defineProperty('bespeak_h', {
					name: 'bespeakHours',
				});

				this.defineProperty('bespeak_min', {
					name: 'bespeakMin',
				});

				this.defineProperty('bespeak_status', {
					name: 'bespeakStatus',
				});

				this.defineProperty('left_time', {
					name: 'timeRemaining', // seconds
					mapper: Number,
				});

				this.defineProperty('run_status', {
					name: 'runStatus',
				});

				this.defineProperty('wash_temp', {
					name: 'relativeTemperature',
				});

				this.defineProperty('custom_program', {
					name: 'customProgram',
					mapper: String
				});

				this.defineProperty('fastwash_drying', {
					name: 'fastwashDrying',
				});

				this.defineProperty('fastwash_dry_t', {
					name: 'fastwashDryT',
				});

				this.defineProperty('temp_info', {
					name: 'tempInfo',
					mapper: (v) => v.split('-').map((i) => parseInt(i)),
				});

				// this.defineProperty('flashdry_interval', {
				//   name: 'flashdryInterval',
				// });

				// this.defineProperty('WaterHardness', {
				//   name: 'waterHardness',
				// });

				// this.defineProperty('city', {
				//   name: 'city',
				// });
			}

			propertyUpdated(key, value, oldValue) {
				super.propertyUpdated(key, value, oldValue);

				if (key === 'program' && value !== '3') {
					this.updateMode(value);
				}

				if (key === 'customProgram' && this.property('program') === '3') {
					this.updateMode(`3|${value}`);
				}
			}

			changeMode(mode) {
				let subMode = null;
				[mode, subMode] = mode.id.split('|');

				return Promise.resolve()
					.then(() => {
						if (subMode) {
							return this.call('set_custom_program', [parseInt(subMode)], {
								refresh: ['customProgram'],
							});
						}
						return Promise.resolve();
					})
					.then(() => {
						return this.call('set_program', [parseInt(mode)], {
							refresh: ['program'],
						});
					});
			}

			activateCleaning() {
				return this.call('set_wash_action', [1], {
					refresh: ['power'],
				});
			}

			deactivateCleaning() {
				return this.call('set_wash_action', [0], {
					refresh: ['power'],
				});
			}
		}
);

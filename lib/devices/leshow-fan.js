const { Fan } = require('abstract-things/climate');

const MiioApi = require('../device');
const {
	ErrorState,
	SwitchablePower,
	SwitchableNightMode,
	AdjustableFanSpeed,
	SwitchableVerticalSwing,
	AdjustableTimer,
	SwitchableAudioFeedback,
} = require('./capabilities');

module.exports = class extends Fan.with(
	ErrorState,
	MiioApi,
	SwitchableAudioFeedback,
	SwitchablePower,
	SwitchableNightMode,
	AdjustableFanSpeed,
	SwitchableVerticalSwing,
	AdjustableTimer
) {
	static get type() {
		return 'miio:fan';
	}

	constructor(options) {
		super(options);

		this.defineProperty('power', {
			mapper: Boolean,
		});

		this.defineProperty('mode', {
			name: 'nightMode',
			mapper: (v) => v === 1,
		});

		this.defineProperty('timer', {
			mapper: (v) => ({
				value: v,
				unit: 'm'
			})
		});

		this.defineProperty('blow', {
			name: 'fanSpeed',
		});

		this.defineProperty('sound', {
			name: 'audioFeedback',
			mapper: Boolean,
		});

		this.defineProperty('yaw', {
			name: 'verticalSwing',
			mapper: Boolean,
		});

		this.defineProperty('fault', {
			name: 'error',
			mapper: (v) => (v === 0 ? null : v),
		});
	}

	changePower(power) {
		return this.call('set_power', [power ? 1 : 0], {
			refresh: ['power', 'nightMode', 'fanSpeed'],
		});
	}

	changeFanSpeed(fanSpeed) {
		return this.call('set_blow', [fanSpeed], {
			refresh: ['power', 'nightMode', 'fanSpeed'],
		});
	}

	changeNightMode(nightMode) {
		const param = nightMode ? 1 : 0;
		return this.call('set_mode', [param], {
			refresh: ['power', 'nightMode', 'fanSpeed'],
		});
	}

	changeAudioFeedback(audioFeedback) {
		const param = audioFeedback ? 1 : 0;
		return this.call('set_sound', [param], {
			refresh: ['audioFeedback'],
		});
	}

	changeVerticalSwing(verticalSwing) {
		const param = verticalSwing ? 1 : 0;
		return this.call('set_yaw', [param], {
			refresh: ['verticalSwing'],
		});
	}

	changeTimer(timer) {
		const param = timer.m; // param in minutes
		return this.call('set_timer', [param], {
			refresh: ['timer'],
		});
	}
};

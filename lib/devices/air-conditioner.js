const { Thermostat } = require('abstract-things/climate');
const MiioApi = require('../device');
const {
	SwitchablePower,
	SwitchableMode,
	SwitchableFanLevel,
	AdjustableTargetTemperature,
	RelativeTemperature,
	RelativeHumidity,
	AdjustableVolume,
	PowerConsumed,
	SwitchableVerticalSwing,
	SwitchableHorizontalSwing,
	SwitchableNightMode,
} = require('./capabilities');

const MODES = [
	{
		id: 'cooling',
		description: 'Cooling',
	},
	{
		id: 'heat',
		description: 'Heating',
	},
	{
		id: 'wind',
		description: 'Fan',
	},
	{
		id: 'arefaction',
		description: 'Dehumidifying',
	},
];

const FAN_LEVELS = [
	{
		id: '0',
		description: '1',
	},
	{
		id: '1',
		description: '2',
	},
	{
		id: '2',
		description: '3',
	},
	{
		id: '3',
		description: '4',
	},
	{
		id: '4',
		description: '5',
	},
	{
		id: '5',
		description: 'Auto',
	},
];

module.exports = class extends Thermostat.with(
	MiioApi,
	SwitchablePower,
	SwitchableMode,
	SwitchableFanLevel,
	AdjustableTargetTemperature,
	RelativeTemperature,
	RelativeHumidity,
	AdjustableVolume,
	PowerConsumed,
	SwitchableVerticalSwing,
	SwitchableHorizontalSwing,
	SwitchableNightMode
) {
	static get type() {
		return 'miio:air-conditioner';
	}

	constructor(options) {
		super(options);

		this.defineProperty('power', (v) => v === 'on');

		this.defineProperty('mode');
		this.updateModes(MODES);

		this.defineProperty('temp_dec', {
			name: 'relativeTemperature',
			mapper: (v) => v / 10.0,
		});

		this.defineProperty('humidity', {
			name: 'relativeHumidity',
		});

		this.defineProperty('volume_level', {
			name: 'volume',
			mapper: (v) => Math.floor((v * 100) / 7),
		});

		this.defineProperty('lcd_level', {
			name: 'ledLevel',
		});

		this.defineProperty('idle_timer', {
			name: 'idleTimer',
		});

		this.defineProperty('open_timer', {
			name: 'openTimer',
		});

		this.defineProperty('st_temp_dec', {
			name: 'targetTemperature',
			mapper: (v) => v / 10.0,
		});
		this.updateTargetTemperatureRange(16, 32);

		this.defineProperty('speed_level', {
			name: 'fanLevel',
			mapper: String,
		});
		this.updateFanLevels(FAN_LEVELS);

		this.defineProperty('vertical_rt', {
			name: 'verticalRt',
		});

		this.defineProperty('ptc', {
			name: 'ptc',
		});

		this.defineProperty('ptc_rt', {
			name: 'ptcRt',
		});

		this.defineProperty('silent', {
			name: 'nightMode',
			mapper: (v) => v === 'on',
		});

		this.defineProperty('vertical_end', {
			name: 'verticalEnd',
		});

		this.defineProperty('vertical_swing', {
			name: 'verticalSwing',
			mapper: (v) => v === 'on',
		});

		this.defineProperty('horizon_swing', {
			name: 'horizontalSwing',
			mapper: (v) => v === 'on',
		});

		this.defineProperty('ele_quantity', {
			name: 'powerConsumed',
			mapper: (v) => v / 10.0,
		});
	}

	changePower(power) {
		return this.call('set_power', [power ? 'on' : 'off'], {
			refresh: ['power', 'mode'],
			refreshDelay: 200,
		});
	}

	changeMode(mode) {
		return this.call('set_mode', [mode], {
			refresh: ['power', 'mode'],
			refreshDelay: 200,
		});
	}

	changeTargetTemperature(targetTemperature) {
		return this.targetTemperatureRange().then((range) => {
			targetTemperature = Math.min(
				Math.max(targetTemperature.celsius, range.min.celsius),
				range.max.celsius
			);
			const param = targetTemperature * 10;
			return this.call('set_temperature', [param], {
				refresh: ['targetTemperature'],
			});
		});
	}

	changeFanLevel(fanLevel) {
		const param = parseInt(fanLevel.id);
		return this.call('set_spd_level', [param], {
			refresh: ['fanLevel'],
		});
	}

	changeVerticalSwing(verticalSwing) {
		const param = verticalSwing ? 'on' : 'off';
		return this.call('set_vertical', [param], {
			refresh: ['verticalSwing'],
		});
	}

	changeHorizontalSwing(horizontalSwing) {
		const param = horizontalSwing ? 'on' : 'off';
		return this.call('set_horizon', [param], {
			refresh: ['horizontalSwing'],
		});
	}

	changeVolume(volume) {
		const param = Math.floor((volume * 100) / 7);
		return this.call('set_volume', [param], {
			refresh: ['volume'],
		});
	}

	changeLedLevel(ledLevel) {
		const param = parseInt(ledLevel.id);
		return this.call('set_lcd_level', [param], {
			refresh: ['ledLevel'],
		});
	}

	changeNightMode(nightMode) {
		const param = nightMode ? 'on' : 'off';
		return this.call('set_silent', [param], {
			refresh: ['nightMode'],
		});
	}
};

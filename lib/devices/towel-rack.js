const { Thing } = require('abstract-things');
const MiioApi = require('../device');
const {
	SwitchablePower,
	SwitchableMode,
	AdjustableTargetTemperature,
	RelativeTemperature,
} = require('./capabilities');

const MODES = [
	{
		id: '3',
		description: 'Programming',
	},
	{
		id: '2',
		description: 'Antifreeze',
	},
	{
		id: '1',
		description: 'Warn',
	},
	{
		id: '0',
		description: 'Dry',
	},
];

module.exports = Thing.type(
	(Parent) =>
		class extends Parent.with(
			MiioApi,
			SwitchablePower,
			SwitchableMode,
			AdjustableTargetTemperature,
			RelativeTemperature
		) {
			static get type() {
				return 'miio:tower-rack';
			}

			constructor(options) {
				super(options);

				this._props = [
					'power',
					'mode',
					'dryTemperature',
					'heatTemperature',
					'dryTime',
					'6',
					'tempSurf',
					'relativeTemperature',
					'powerPercent',
					'10',
				];

				this.defineProperty('power', {
					mapper: Boolean,
				});

				this.defineProperty('mode', {
					mapper: String,
				});
				this.updateModes(MODES);

				this.defineProperty('relativeTemperature', {
					mapper: (v) => parseInt(v) / 2,
				});

				this.defineProperty('dryTemperature', {
					mapper: (v) => parseInt(v) / 2,
				});

				this.defineProperty('heatTemperature', {
					mapper: (v) => parseInt(v) / 2,
				});
			}

			loadProperties(props) {
				props = props.map(
					(key) => this._reversePropertyDefinitions[key] || key
				);

				return this.call('get_props', []).then((result) => {
					const obj = {};
					for (let i = 0; i < props.length; i++) {
						const prop = props[i];
						const j = this._props.indexOf(prop);
						this._pushProperty(obj, prop, j === -1 ? null : result[j]);
					}
					return obj;
				});
			}

			propertyUpdated(key, value, oldValue) {
				super.propertyUpdated(key, value, oldValue);

				this.mode().then((mode) => {
					if (key === 'dryTemperature' && mode.id === '0') {
						this.updateTargetTemperature(value);
					} else if (key === 'heatTemperature' && mode.id === '1') {
						this.updateTargetTemperature(value);
					} else {
						this.updateTargetTemperature(null);
					}
				});
			}

			changePower(power) {
				const param = power ? 1 : 0;
				return this.call('set_power', [param], {
					refresh: ['power'],
				});
			}

			changeMode(mode) {
				const param = parseInt(mode.id);
				return this.call('set_mode', [param], {
					refresh: ['mode'],
				});
      }
      
      changeTargetTemperature(targetTemperature) {
        return this.mode().then((mode) => {
          const param = parseInt(targetTemperature.celsius) * 2;
					if (mode.id === '0') {
						return this.call('set_tempdry', [param], {
              refresh: ['mode', 'dryTemperature'],
            });
					} else if (mode.id === '1') {
						return this.call('set_tempheat', [param], {
              refresh: ['mode', 'heatTemperature'],
            });
					}
        });
      }
		}
);

// data	{"id":34,"method":"set_drytime","accessKey":"IOS00026747c5acafc2","params":[5]}
// {"method":"set_temprog","params":[4128768]}

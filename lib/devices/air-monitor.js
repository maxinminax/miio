const { ChargingState } = require('abstract-things');
const { AirMonitor } = require('abstract-things/climate');
const MiioApi = require('../device');
const { SwitchablePower, BatteryLevel, PM2_5 } = require('./capabilities');

module.exports = class extends AirMonitor.with(
	MiioApi,
	SwitchablePower,
	PM2_5,
	BatteryLevel,
	ChargingState
) {
	static get type() {
		return 'miio:air-monitor';
	}

	constructor(options) {
		super(options);

		// Define the power property
		this.defineProperty('power', (v) => v === 'on');

		// Sensor value used for AQI (PM2.5) capability
		this.defineProperty('aqi', {
			name: 'pm2_5',
		});

		this.defineProperty('battery', {
			name: 'batteryLevel',
		});

		this.defineProperty('usb_state', {
			name: 'charging',
			mapper: (v) => v === 'on',
		});
	}

	propertyUpdated(key, value, oldValue) {
		if (key === 'charging') {
			this.updateCharging(value);
		}

		super.propertyUpdated(key, value, oldValue);
	}

	changePower(power) {
		return this.call('set_power', [power ? 'on' : 'off'], {
			refresh: ['power', 'mode'],
			refreshDelay: 200,
		});
	}
};

const { Thing } = require('abstract-things');
const MiioApi = require('../device');

module.exports = Thing.type(Parent => class extends Parent.with(MiioApi) {
  static get type() {
    return 'miio:viomi-juicer';
  }

  constructor(options) {
    super(options);
    
    this.defineProperty('run_status', {
      name: 'runStatus'
    });
    
    this.defineProperty('work_status', {
      name: 'workStatus'
    });
    
    this.defineProperty('mode', {
    });
    
    this.defineProperty('cook_status', {
      name: 'cookStatus'
    });
    
    this.defineProperty('warm_time', {
      name: 'warmTime'
    });
    
    this.defineProperty('cook_time', {
      name: 'cookTime'
    });
    
    this.defineProperty('left_time', {
      name: 'leftTime'
    });
    
    this.defineProperty('cooked_time', {
      name: 'cookedTime'
    });
    
    this.defineProperty('curr_tempe', {
      name: 'temperature'
    });
    
    this.defineProperty('mode_sort', {
      name: 'modeSort'
    });
    
    this.defineProperty('rev', {
      name: 'reverse'
    });
    
    this.defineProperty('version', {
      name: 'version'
    });
    
    this.defineProperty('stand_top_num', {
      name: 'standTopNum'
    });
    
    this.defineProperty('warm_data', {
      name: 'warmData'
    });
    
    this.defineProperty('hard_version', {
      name: 'hardwareVersion'
    });
  }

  propertyUpdated(key, value, oldValue) {
    super.propertyUpdated(key, value, oldValue);
  }
});
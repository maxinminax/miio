const _ = require('lodash');
const { Thing } = require('abstract-things');

const Device = require('./device');

module.exports = Thing.type(Parent => class extends Parent.with(Device) {
  static get type() {
    return 'miot';
  }

  constructor(handle, spec) {
    super(handle);

    this._propertiesMapping = spec ? this.parseSpec(spec) : {};
  }
  
  parseSpec(spec) {
    const { services } = spec;
    return _.keyBy(_.concat(..._.map(services, service => {
      const { properties } = service;
      return _.map(properties, property => {
        return {
          type: [service.type.split(':')[3], property.type.split(':')[3]].join(':'),
          siid: service.iid,
          piid: property.iid,
          // desc: property.description
        }; 
      });
    })), 'type');
  }
  
  miotCall(method, args, options) {
    args = args.map(arg => Object.assign({}, arg, { did: this.handle.api.id }));
    return this.call(method, args, options);
  }

  loadProperties(props) {
    props = props
      .map(key => this._reversePropertyDefinitions[key] || key);
    
    return this
      .call('get_properties', props.map(key => {
        const did = this.handle.api.id;
        const { siid, piid } = this._propertiesMapping[key];
        return { did, siid, piid };
      }))
      .then(result => {
        const obj = {};
        for(let i=0; i<result.length; i++) {
          // TODO: check did, siid, piid, code
          this._pushProperty(obj, props[i], result[i].code === 0 ? result[i].value : null);
        }
        return obj;
      });
  }
  
  changeProperty(key, value, options) {
    const did = this.handle.api.id;
    const name = this._reversePropertyDefinitions[key] || key;
    const { siid, piid } = this._propertiesMapping[name];
    options = options || {
      refresh: [key]
    };
    return this.call('set_properties', [{ did, siid, piid, value }], options);
  }

  static checkOk(result) {
    if (result && result[0].code === 0) {
      return null;
    }

    throw new Error('Could not perform operation');
  }
});